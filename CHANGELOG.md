# Changelog

## Unreleased

## Fixed
- centrer verticalement l'image dans la bannière de la page d'accueil

## 1.0.2 - 2023-07-25

## Fixed
- Ne pas afficher le texte du modèle `icone` et le mettre en `title` du lien

### Changed
- mise à jour du README.md maintenant que la doc est sur contrib
## 1.0.1 - 2023-06-22

### Added
- ajout d'un CHANGELOG.md

### Changed

- compatibilité SPIP 4.1+
- mise à jour de la documentation dans le README.md