<?php
 
// Sécurité
if (!defined("_ECRIRE_INC_VERSION")) return;
 
function html5up_twenty_declarer_champs_extras($champs = array()) {
 
    // Input pour choisir le picto Fontawsome dans l'entête des rubriques et articles
    $champs['spip_articles']['picto_fa'] = array(
        'saisie' => 'input',
        'options' => array(
            'nom' => 'picto_fa',
            'label' => _T('html5up_twenty:picto_fa'),
            'explication' => _T('html5up_twenty:picto_fa_explication'),
            'placeholder' => _T('html5up_twenty:picto_fa_placeholder'), 
            'defaut' => '',
            'rows' => 10,
            'sql' => "text DEFAULT '' NOT NULL",
        )
    );
    $champs['spip_rubriques']['picto_fa'] = array(
        'saisie' => 'input',
        'options' => array(
            'nom' => 'picto_fa',
            'label' => _T('html5up_twenty:picto_fa'), 
            'explication' => _T('html5up_twenty:picto_fa_explication'),
            'placeholder' => _T('html5up_twenty:picto_fa_placeholder'), 
            'defaut' => '',
            'rows' => 10,
            'sql' => "text DEFAULT '' NOT NULL",
        )
    );
    return $champs;	
}